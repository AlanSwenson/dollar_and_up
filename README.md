# About this project (it's June 11, 2020 almost midnight)
This is the beginnings of Dollar & Up's website.

The **primary goal** is to get a _static_ website up ASAP hosted with a nice domain name and a link on the Google maps location to it.

Last week, I met Harris who owns/runs the store and he said a website for his store would be very helpful.  From the sound of it, he's had a rough time in the past few months getting customers because of the shutdown due to COVID-19 and then also being right on 38th street near the cross-roads where George Floyd was murdered (everyone's taking Instagram photos a block down and gettin free hot dogs from other douches instead of buying shit from his shop).

I'm hoping we can setup up a nice website for his business that can be linked to the location on Google maps to help out his SEO, which adds good layer of marketing/visibility/extra legitimacy.  The business is located at:

	https://goo.gl/maps/EXL6VvBG4kGiz7Xg6
	3653 Chicago Ave. S.
	Minneapolis MN 55407

I've included an image of the store front and a log designed(picture of a t-shirt) in the directory `content/images/` which give an idea of where/what the store is about.  The shop provides many products to the surrounding community.  These include Groceries & toiletries and is an all-around convenience store with clothes and smoking accessories too.

# What I'm doing for this effort
In general, I'm managing the entire project and coordinating directly with the store owner and anyone else working on the project.  I'm a backend developer by trade, so I'll be handling all of the deployment/infrastructure of the site also.  If you're curious about how that all works, feel free to harass me about it :)

I'm going to be getting the domain names, SSL cert, & an extremely simple deployment this weekend (It's June 12th 2020 around 01:00 right meow)

# Get started
Clone this repo to your computer or use the Gitlab UI to edit files! ... Not everyone is a nerdy programmer so hit me up if that seems like a pain in the ass :)

ryandavisbusiness@gmail.com or text me @ 720-352-7320

Overall, the `terraform/` directory is the infrastructure code which can be used to deploy the site and the `content/` directory is all the files that would usually define a site... to see/modify the site, open `content/index.html` in a browser.  What you see in that browser is what would be seen on the deployed site (pretty weak right now, yah?)

Look at the next section to find something to help out with!

# Current work TODO
These are things I need help with.  I can do these things myself, but will likely do them badly :D  I think many of these could take about an hour or 2 if they're in your respective wheelhouse.  Also, if there are other ideas or tasks you think should be added, feel free to make merge requests or contact me.

* Marketing
	* Write a blurb for the "About" section
		* write 1-2 paragraphs about the business for website content
		* Also, I'll be coordinating with Harris about this and can communicate with him
		* Make a merge request into the `content/images/marketing/` directory
	* Get good quality pictures of the products for display on the website
		* This needs to wait until we have a good proof of concept to show Harris
		* Make a merge request into the `content/images/marketing/` directory


* Graphic Design
	* Come up with some color schemes for the site
		* Look in the `content/images/` directory and use the banner from the store front and/or designed log to generate ideas or come up with something completely different
		* Drop the color schemes and their descriptions into the `content/graphic_design` directory and make a merge request
  * Come up with a font or fonts
		* Look in the `content/images/` directory and use Harris' store front or logo to generate ideas or come up with something completely different
		* Drop ideas and their descriptions into the `content/graphic_design` directory and make a merge request
	* Come up with a layout design for the website as a whole
		* This can be anything; pictures, HTML, whatever
			* Drop ideas into the `content/marketing` or `content/graphic_design/` directories and make a merge request


* Frontend Dev
	* Fixup `content/index.html`
		* this is a hot mess currently. Populate it, structure it, do what you will... this serves as the base content for the website.
  * Fixup `content/all.css`
		* there is nothing in there currently.  Add some basic formatting/styling, use it in `index.html`
  * Figure out how to display different pages when navigation buttons are pressed (javascripty stuff)
    * There's going to be several sections such as an "About", "Products", "Contacts" maybe others that will need to be navigated to via these buttons

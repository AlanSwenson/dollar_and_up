terraform {
  backend s3 {
    bucket = "dollarandup-terraform-state"
    key    = "state"
  }
}

provider "aws" {
  version = "~> 2.0"
}

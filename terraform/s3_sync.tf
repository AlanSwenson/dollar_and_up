locals {
  content_path = "../content/"
  bucket_uri   = "s3://${aws_s3_bucket.www.bucket}"

  www_cache_id     = aws_cloudfront_distribution.www_distribution.id
  no_www_cache_id  = aws_cloudfront_distribution.root_distribution.id
}

data archive_file website_content {
  type        = "zip"
  source_dir = local.content_path
  output_path = "s3_sync_sha_trigger_file"
}

resource null_resource sync_content_dir_to_s3_bucket {
  depends_on = [data.archive_file.website_content]
  triggers = {
    src_hash = data.archive_file.website_content.output_sha
  }

  provisioner local-exec {
    command = "aws s3 sync --delete ${local.content_path} ${local.bucket_uri}"
  }
}

//Invalidate CDN caches if we updated site
resource null_resource invalidate_www_cache {
  depends_on = [data.archive_file.website_content]
  triggers = {
    src_hash = data.archive_file.website_content.output_sha
  }
  provisioner local-exec {
    command = "aws cloudfront create-invalidation --distribution-id ${local.www_cache_id} --paths \"/*\""
  }
}
resource null_resource invalidate_no_www_cache {
  depends_on = [data.archive_file.website_content]
  triggers = {
    src_hash = data.archive_file.website_content.output_sha
  }
  provisioner local-exec {
    command = "aws cloudfront create-invalidation --distribution-id ${local.no_www_cache_id} --paths \"/*\""
  }
}
